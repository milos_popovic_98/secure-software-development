package com.zuehlke.securesoftwaredevelopment.repository;

import com.zuehlke.securesoftwaredevelopment.config.AuditLogger;
import com.zuehlke.securesoftwaredevelopment.config.Entity;
import com.zuehlke.securesoftwaredevelopment.domain.Address;
import com.zuehlke.securesoftwaredevelopment.domain.Food;
import com.zuehlke.securesoftwaredevelopment.domain.FoodItem;
import com.zuehlke.securesoftwaredevelopment.domain.NewOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderRepository {
    private static final Logger LOG = LoggerFactory.getLogger(OrderRepository.class);
    private static final AuditLogger auditLogger = AuditLogger.getAuditLogger(OrderRepository.class);

    private DataSource dataSource;

    public OrderRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public List<Food> getMenu(int id) {
        List<Food> menu = new ArrayList<>();
        String sqlQuery = "SELECT id, name FROM food WHERE restaurantId=" + id;
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(sqlQuery)) {
            while (rs.next()) {
                menu.add(createFood(rs));
            }
            LOG.info("Got menu(restaurant_id: {}, size: {})", id, menu.size());
        } catch (SQLException e) {
            LOG.warn("Getting menu(restaurant_id: {} failed.", id, e);
        }

        return menu;
    }

    private Food createFood(ResultSet rs) throws SQLException {
        int id = rs.getInt(1);
        String name = rs.getString(2);
        return new Food(id, name);
    }

    public void insertNewOrder(NewOrder newOrder, int userId) {
        LocalDate date = LocalDate.now();
        String delivery = "INSERT INTO delivery (isDone, userId, restaurantId, addressId, date, comment)" +
                "values (?, ?, ?, ?, ?, ?)";
        String deliveryItems = "INSERT INTO delivery_item (amount, foodId, deliveryId) VALUES(?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement deliveryStatement = connection.prepareStatement(delivery, PreparedStatement.RETURN_GENERATED_KEYS);
             PreparedStatement itemsStatement = connection.prepareStatement(deliveryItems)) {

            deliveryStatement.setBoolean(1, false);
            deliveryStatement.setInt(2, userId);
            deliveryStatement.setInt(3, newOrder.getRestaurantId());
            deliveryStatement.setInt(4, newOrder.getAddress());
            deliveryStatement.setString(5, date.getYear() + "-" + date.getMonthValue() + "-" + date.getDayOfMonth());
            deliveryStatement.setString(6, newOrder.getComment());

            deliveryStatement.executeUpdate();
            ResultSet generatedKeys = deliveryStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int deliveryId = generatedKeys.getInt(1);
                for (FoodItem item : newOrder.getItems()) {
                    itemsStatement.setInt(1, item.getAmount());
                    itemsStatement.setInt(2, item.getFoodId());
                    itemsStatement.setInt(3, deliveryId);

                    itemsStatement.addBatch();
                }
                itemsStatement.executeBatch();
                auditLogger.auditChange(new Entity("Order.create",
                        String.valueOf(deliveryId),
                        null,
                        newOrder.toString()));
            }
        } catch (SQLException e) {
            LOG.warn("Creating order failed.", e);
        }
    }

    public Object getAddresses(int userId) {
        List<Address> addresses = new ArrayList<>();
        String sqlQuery = "SELECT id, name FROM address WHERE userId=" + userId;
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(sqlQuery)) {
            while (rs.next()) {
                addresses.add(createAddress(rs));
            }
            LOG.info("Got {} addreses, user_id = {}", addresses.size(), userId);
        } catch (SQLException e) {
            LOG.warn("Getting addresses failed.", e);
        }
        return addresses;
    }

    private Address createAddress(ResultSet rs) throws SQLException {
        int id = rs.getInt(1);
        String name = rs.getString(2);
        return new Address(id, name);

    }
}
