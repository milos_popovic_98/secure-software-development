package com.zuehlke.securesoftwaredevelopment.controller;

import com.zuehlke.securesoftwaredevelopment.config.AuditLogger;
import com.zuehlke.securesoftwaredevelopment.domain.RestaurantUpdate;
import com.zuehlke.securesoftwaredevelopment.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RestaurantController {

    private static final Logger LOG = LoggerFactory.getLogger(RestaurantController.class);
    private static final AuditLogger auditLogger = AuditLogger.getAuditLogger(RestaurantController.class);

    private final CustomerRepository customerRepository;

    @Autowired
    public RestaurantController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/restaurants")
    @PreAuthorize("hasAuthority('RESTAURANT_LIST_VIEW')")
    public String restaurants(Model model) {
        LOG.info("Get restaurants called.");
        model.addAttribute("restaurants", customerRepository.getRestaurants());
        return "restaurants";
    }

    @GetMapping("/restaurant")
    @PreAuthorize("hasAuthority('RESTAURANT_DETAILS_VIEW')")
    public String getRestaurant(@RequestParam(name = "id", required = true) String id, Model model) {
        LOG.info("Get restaurant(id: {}) called.", id);
        model.addAttribute("restaurant", customerRepository.getRestaurant(id));
        return "restaurant";
    }

    @DeleteMapping("/restaurant")
    @PreAuthorize("hasAuthority('RESTAURANT_DELETE')")
    public String deleteRestaurant(@RequestParam(name = "id", required = true) String id) {
        int identificator = Integer.valueOf(id);
        auditLogger.audit(String.format("Delete restaurant(id: %s) called.", id));
        customerRepository.deleteRestaurant(identificator);
        return "/restaurants";
    }

    @PostMapping("/api/restaurant/update-restaurant")
    @PreAuthorize("hasAuthority('RESTAURANT_EDIT')")
    public String updateRestaurant(RestaurantUpdate restaurantUpdate, Model model) {
        auditLogger.audit(String.format("Update restaurant(data: %s) called.", restaurantUpdate.toString()));
        customerRepository.updateRestaurant(restaurantUpdate);
        restaurants(model);
        return "/restaurants";
    }

}
