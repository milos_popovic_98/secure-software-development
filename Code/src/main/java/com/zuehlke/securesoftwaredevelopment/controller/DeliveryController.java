package com.zuehlke.securesoftwaredevelopment.controller;

import com.zuehlke.securesoftwaredevelopment.domain.DeliveryDetail;
import com.zuehlke.securesoftwaredevelopment.domain.ViewableDelivery;
import com.zuehlke.securesoftwaredevelopment.repository.DeliveryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class DeliveryController {
    private static final Logger LOG = LoggerFactory.getLogger(DeliveryController.class);

    private final DeliveryRepository deliveryRepository;

    public DeliveryController(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    @GetMapping("/")
    public String showDeliveries(Model model) {
        LOG.info("Show deliveries called.");
        model.addAttribute("deliveries", deliveryRepository.getAllDeliveries());
        return "deliveries";
    }

    @GetMapping("/delivery")
    public String showDelivery(@RequestParam(name = "id", required = true) String id, Model model) {
        LOG.info("Show delivery(id: {}) called.", id);
        model.addAttribute("delivery", deliveryRepository.getDelivery(id));
        List<DeliveryDetail> details = deliveryRepository.getDeliveryDetails(id);
        model.addAttribute("details", details);
        model.addAttribute("sum", deliveryRepository.calculateSum(details));
        return "delivery";
    }

    @GetMapping(value = "/api/deliveries/search", produces = "application/json")
    @ResponseBody
    public List<ViewableDelivery> search(@RequestParam("query") String query) {
        LOG.info("Search delivery called - query = {}", query);
        return deliveryRepository.search(query);
    }

}
