package com.zuehlke.securesoftwaredevelopment.controller;

import com.zuehlke.securesoftwaredevelopment.config.AuditLogger;
import com.zuehlke.securesoftwaredevelopment.domain.Food;
import com.zuehlke.securesoftwaredevelopment.domain.NewOrder;
import com.zuehlke.securesoftwaredevelopment.domain.User;
import com.zuehlke.securesoftwaredevelopment.repository.CustomerRepository;
import com.zuehlke.securesoftwaredevelopment.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class OrderController {
    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);
    private static final AuditLogger auditLogger = AuditLogger.getAuditLogger(OrderController.class);

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;

    public OrderController(OrderRepository orderRepository, CustomerRepository customerRepository) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
    }

    @GetMapping("/order")
    @PreAuthorize("hasAuthority('ORDER_FOOD')")
    public String order(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();

        LOG.info("Show restaurants and addresses called by user {}.", user.getId());
        model.addAttribute("restaurants", customerRepository.getRestaurants());
        model.addAttribute("addresses", orderRepository.getAddresses(user.getId()));
        return "order";
    }


    @GetMapping(value = "/api/menu", produces = "application/json")
    @PreAuthorize("hasAuthority('ORDER_FOOD')")
    @ResponseBody
    public List<Food> getMenu(@RequestParam(name = "id") String id) {
        int identificator = Integer.valueOf(id);
        LOG.info("Show menu called.");
        return orderRepository.getMenu(identificator);
    }

    @PostMapping(value = "/api/new-order", consumes = "application/json")
    @PreAuthorize("hasAuthority('ORDER_FOOD')")
    @ResponseBody
    public String newOrder(@RequestBody NewOrder newOrder) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();

        auditLogger.audit(String.format("Creating new order called (order data: %s) by user %d.", newOrder, user.getId()));
        orderRepository.insertNewOrder(newOrder, user.getId());
        return "";
    }
}
